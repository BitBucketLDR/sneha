/**
 *
 */
package org.paytm.storefront.customerregistrationvalidator;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.paytm.storefront.myprofileform.MyProfileForm;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * @author DELL
 *
 */
@Component("updateProfileValidator")
public class UpdateProfileValidator implements Validator
{



	@Override
	public void validate(final Object object, final Errors errors)
	{
		final MyProfileForm profileForm = (MyProfileForm) object;
		//		final String dateofBirth = profileForm.getDateofBirth();
		final String phoneNumber = profileForm.getPhoneNumber();
		//		final Integer age = profileForm.getAge();


		if (StringUtils.isEmpty(phoneNumber))
		{

			errors.rejectValue("phoneNumber", "register.phoneNumber.Empty");

		}
		final Pattern pattern = Pattern.compile("[0-9]{10}");
		final Matcher matcher = pattern.matcher(phoneNumber);
		if (!matcher.matches())
		{
			errors.rejectValue("phoneNumber", "register.phoneNumber.invalid");
		}

	}

	/*
	 * if (StringUtils.isEmpty(dateofBirth)) { errors.rejectValue("dateofBirth", "register.dateofBirth.Empty");
	 *
	 * } final Pattern pattern1 = Pattern.compile("^[0-9]{4}/(1[0-2]|0[1-9])/(3[01]|[12][0-9]|0[1-9])$"); final Matcher
	 * matcher1 = pattern1.matcher(dateofBirth); if (!matcher1.matches()) { errors.rejectValue("dateofBirth",
	 * "register.dateofBirth.invalid"); } }
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.validation.Validator#supports(java.lang.Class)
	 */

	@Override
	public final boolean supports(final Class<?> aClass)
	{
		return MyProfileForm.class.equals(aClass);
	}
}

