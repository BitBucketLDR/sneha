/**
 *
 */
package org.paytm.storefront.customerregistrationvalidator;

import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.RegistrationValidator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.paytm.storefront.myform.MyForm;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;


@Component("customerRegistrationValidator")
public class CustomerRegistrationValidator extends RegistrationValidator
{




	@Override
	public void validate(final Object object, final Errors errors)
	{
		final MyForm myform = (MyForm) object;
		final String titleCode = myform.getTitleCode();
		final String firstName = myform.getFirstName();
		final String lastName = myform.getLastName();
		final String email = myform.getEmail();
		final String pwd = myform.getPwd();
		final String phoneNumber = myform.getPhoneNumber();
		final String dateofBirth = myform.getDateofBirth();
		/* final Integer age = myform.getAge(); */
		final String checkPwd = myform.getCheckPwd();

		validateTitleCode(errors, titleCode);
		validateName(errors, firstName, "firstName", "register.firstName.invalid");
		validateName(errors, lastName, "lastName", "register.lastName.invalid");

		if (StringUtils.length(firstName) + StringUtils.length(lastName) > 255)
		{
			errors.rejectValue("lastName", "register.name.invalid");
			errors.rejectValue("firstName", "register.name.invalid");
		}

		validateEmail(errors, email);
		validatePassword(errors, pwd);
		comparePasswords(errors, pwd, checkPwd);
		validatephoneNumber(phoneNumber, errors);
		validatedateofBirth(dateofBirth, errors);
		/* validateage(age, errors); */


	}



	private void validatephoneNumber(final String phoneNumber, final Errors errors)
	{
		if (StringUtils.isEmpty(phoneNumber))
		{

			errors.rejectValue("phoneNumber", "register.phoneNumber.Empty");

		}
		final Pattern pattern = Pattern.compile("[0-9]{10}");
		final Matcher matcher = pattern.matcher(phoneNumber);
		if (!matcher.matches())
		{
			errors.rejectValue("phoneNumber", "register.phoneNumber.invalid");
		}
	}


	private void validatedateofBirth(final String dateofBirth, final Errors errors)
	{
		if (StringUtils.isEmpty(dateofBirth))
		{
			errors.rejectValue("dateofBirth", "register.dateofBirth.Empty");

		}
		final Pattern pattern = Pattern.compile("^[0-9]{4}/(1[0-2]|0[1-9])/(3[01]|[12][0-9]|0[1-9])$");
		final Matcher matcher = pattern.matcher(dateofBirth);
		if (!matcher.matches())
		{
			errors.rejectValue("dateofBirth", "register.dateofBirth.invalid");
		}
	}
}

