/**
 *
 */
package org.paytm.storefront.myprofileform;

import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdateProfileForm;


/**
 * @author DELL
 *
 */
public class MyProfileForm extends UpdateProfileForm
{
	private String dateofBirth;
	private String phoneNumber;
	private Integer age;

	/**
	 * @return the dateofBirth
	 */
	public String getDateofBirth()
	{
		return dateofBirth;
	}

	/**
	 * @param dateofBirth
	 *           the dateofBirth to set
	 */
	public void setDateofBirth(final String dateofBirth)
	{
		this.dateofBirth = dateofBirth;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	/**
	 * @param phoneNumber
	 *           the phoneNumber to set
	 */
	public void setPhoneNumber(final String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the age
	 */
	public Integer getAge()
	{
		return age;
	}

	/**
	 * @param age
	 *           the age to set
	 */
	public void setAge(final Integer age)
	{
		this.age = age;
	}

}
