/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Jan 8, 2019 9:42:09 AM                      ---
 * ----------------------------------------------------------------
 */
package org.paytm.initialdata.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedPaytmInitialDataConstants
{
	public static final String EXTENSIONNAME = "paytminitialdata";
	
	protected GeneratedPaytmInitialDataConstants()
	{
		// private constructor
	}
	
	
}
