/**
 *
 */
package org.paytm.facades.populators;



import de.hybris.platform.commercefacades.user.converters.populator.CustomerPopulator;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.user.CustomerModel;


/**
 * @author DELL
 *
 */
public class NewCustomerPopulator extends CustomerPopulator
{
	@Override
	public void populate(final CustomerModel source, final CustomerData target)
	{


		super.populate(source, target);

		if (source.getDateofBirth() != null)
		{
			target.setDateofBirth(source.getDateofBirth());
		}
		if (source.getPhoneNumber() != null)
		{
			target.setPhoneNumber(source.getPhoneNumber());
		}
		if (source.getAge() != null)
		{
			target.setAge(source.getAge());
		}
	}

}

