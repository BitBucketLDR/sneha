/**
 *
 */
package org.paytm.facades.customerfacadesimpl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.commercefacades.customer.impl.DefaultCustomerFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.TitleModel;

import org.apache.commons.lang.StringUtils;
import org.paytm.facades.populators.NewCustomerPopulator;
import org.springframework.util.Assert;


/**
 * @author DELL
 *
 */
public class CustomerFacadesImpl extends DefaultCustomerFacade
{
	private NewCustomerPopulator newCustomerPopulator;

	@Override
	public void register(final RegisterData registerData) throws DuplicateUidException
	{
		validateParameterNotNullStandardMessage("registerData", registerData);
		Assert.hasText(registerData.getFirstName(), "The field [FirstName] cannot be empty");
		Assert.hasText(registerData.getLastName(), "The field [LastName] cannot be empty");
		Assert.hasText(registerData.getLogin(), "The field [Login] cannot be empty");

		final CustomerModel newCustomer = getModelService().create(CustomerModel.class);
		newCustomer.setName(getCustomerNameStrategy().getName(registerData.getFirstName(), registerData.getLastName()));

		if (StringUtils.isNotBlank(registerData.getFirstName()) && StringUtils.isNotBlank(registerData.getLastName()))
		{
			newCustomer.setName(getCustomerNameStrategy().getName(registerData.getFirstName(), registerData.getLastName()));
		}
		final TitleModel title = getUserService().getTitleForCode(registerData.getTitleCode());
		newCustomer.setTitle(title);
		setUidForRegister(registerData, newCustomer);
		newCustomer.setSessionLanguage(getCommonI18NService().getCurrentLanguage());
		newCustomer.setSessionCurrency(getCommonI18NService().getCurrentCurrency());
		newCustomer.setAge(registerData.getAge());
		newCustomer.setDateofBirth(registerData.getDateofBirth());
		newCustomer.setPhoneNumber(registerData.getPhoneNumber());
		getCustomerAccountService().register(newCustomer, registerData.getPassword());
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.commercefacades.customer.impl.DefaultCustomerFacade#updateProfile(de.hybris.platform.
	 * commercefacades.user.data.CustomerData)
	 */
	@Override
	public void updateProfile(final CustomerData customerData) throws DuplicateUidException
	{

		validateDataBeforeUpdate(customerData);

		final String name = getCustomerNameStrategy().getName(customerData.getFirstName(), customerData.getLastName());
		final CustomerModel customer = getCurrentSessionCustomer();
		customer.setOriginalUid(customerData.getDisplayUid());
		customer.setDateofBirth(customerData.getDateofBirth());
		customer.setPhoneNumber(customerData.getPhoneNumber());
		System.out.println("the correct date of birth of mine in the customer model is :" + customer.getDateofBirth());


		getCustomerAccountService().updateProfile(customer, customerData.getTitleCode(), name, customerData.getUid());

	}


	/**
	 * @return the newCustomerPopulator
	 */
	public NewCustomerPopulator getNewCustomerPopulator()
	{
		return newCustomerPopulator;
	}

	/**
	 * @param newCustomerPopulator
	 *           the newCustomerPopulator to set
	 */
	public void setNewCustomerPopulator(final NewCustomerPopulator newCustomerPopulator)
	{
		this.newCustomerPopulator = newCustomerPopulator;
	}

}
