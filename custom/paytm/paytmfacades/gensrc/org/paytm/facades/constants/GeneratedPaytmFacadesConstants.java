/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Jan 8, 2019 9:42:09 AM                      ---
 * ----------------------------------------------------------------
 */
package org.paytm.facades.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedPaytmFacadesConstants
{
	public static final String EXTENSIONNAME = "paytmfacades";
	
	protected GeneratedPaytmFacadesConstants()
	{
		// private constructor
	}
	
	
}
