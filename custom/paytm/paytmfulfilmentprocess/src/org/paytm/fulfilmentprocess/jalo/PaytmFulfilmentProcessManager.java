/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package org.paytm.fulfilmentprocess.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.paytm.fulfilmentprocess.constants.PaytmFulfilmentProcessConstants;

@SuppressWarnings("PMD")
public class PaytmFulfilmentProcessManager extends GeneratedPaytmFulfilmentProcessManager
{
	public static final PaytmFulfilmentProcessManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (PaytmFulfilmentProcessManager) em.getExtension(PaytmFulfilmentProcessConstants.EXTENSIONNAME);
	}
	
}
